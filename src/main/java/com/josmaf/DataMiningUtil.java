package com.josmaf;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * Utility class for loading YELP dataset 
 * 
 * @author jose
 *
 */
public class DataMiningUtil {
	
	public static final String DRIVER = "com.mysql.jdbc.Driver";
	public static final String URL = "jdbc:mysql://localhost:3306/datamining";
	public static final String USER = "e2e"; 
	public static final String PASS = "e2e";

	public static void main(String[] args) {
				
		//DataMiningUtil.readReviews(3);
		
		DataMiningUtil. loadRestaurantsFromFile();
		
		/* dm.writeToFile(dm.getReviews(),  "D:\\DataMiningCapstone\\"
				+ 	"yelp_dataset_challenge_academic_dataset\\"
				+ 	"sampleRestaurants.txt");  */
		
	} // END main

	
	private DataMiningUtil() {
	  }
	
	/**
	 * Generic method to insert data in DB	 
	 * 
	 * @param tableName Table name
	 * @param columns Array containing field names
	 * @param lists Array of lists. All list must have the same length
	 */
	public static void insertData(String tableName, String[] columns, 
			List<ArrayList<String>> lists) {    
		Connection conn = null;    	
		PreparedStatement preparedStmt = null;
		try {

			Class.forName(DRIVER);
			conn = DriverManager
					.getConnection(URL,USER, PASS);
			StringBuilder sb = new StringBuilder();
			for (int i=0; i<columns.length;i++) {
				if (sb.length()>0) {
					sb.append(",");
				}
				sb.append("?");
			}					
			// prepare query
			String query = "insert into "+ tableName + " ("
					+ String.join(",", columns)
					+") values (" + sb.toString() + ")";

			ArrayList<String> firstList = lists.get(0);

			for (int j=0;j<firstList.size();j++) {
				// create the mysql insert preparedstatement
				preparedStmt = conn.prepareStatement(query);
				for (int i= 0; i<columns.length;i++) {
					preparedStmt.setString(i+1, lists.get(i).get(j));
				}			   
				// execute the preparedstatement
				preparedStmt.execute();
				
				if (j % 100000 == 0) {
					conn.close();
					conn = DriverManager
							.getConnection(URL,USER, PASS);
				}				
			}
			conn.close();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	} // method end


	/**
	 *  Read from business JSON file, filter by categories (), 
	 *  and  insert in "restaurants" table.
	 */
	public static void loadRestaurantsFromFile() {   	    	

		JSONParser parser = new JSONParser();

		String file =  "D:\\DataMiningCapstone\\"
				+ 	"yelp_dataset_challenge_academic_dataset\\"
				+ 	"yelp_academic_dataset_business.json";

		ArrayList<String> businessList = new ArrayList<String>();
		ArrayList<ArrayList<String>> listsSet = 
									new  ArrayList<ArrayList<String>>();	
		String business_id = null;

		try (BufferedReader br = new BufferedReader(new FileReader(file))) {

			String line;
			// Read line by line
			while ((line = br.readLine()) != null) {
				// process the line           	
				JSONObject jsonObject = (JSONObject) parser.parse(line);
				business_id = (String) jsonObject.get("business_id");
				JSONArray categories = (JSONArray) jsonObject.get("categories");
				if (categories != null &&  !categories.isEmpty() 
						&& categories.toString().contains("Restaurants")) { 					
						businessList.add(business_id.trim());
				}
			}
			
			// insert list in restaurants table
			listsSet.add(businessList);
			DataMiningUtil.insertData("restaurants", new String[]{"business_id"}, listsSet);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}    	
	} // method end


	/**
	 *  Write a file based on a list. Each element of the list will be a line in file.
	 * @param list List of lines
	 * @param fileName File name
	 */
	public static void writeToFile(List<String> list, String fileName) {

		File fout = new File(fileName);

		try {
			FileOutputStream fos = new FileOutputStream(fout);	 
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
			for(String line : list) {
				bw.write(line);				
			}
			bw.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	} // method end


	/**
	 * Reads reviews of restaurants from database (reviews_restaurants table)
	 * @return List of reviews
	 */
	public static ArrayList<String> getReviews() {

		Connection conn = null;    	
		ArrayList<String> reviews = new ArrayList<String>();
		String review = null;
		try {

			Class.forName(DRIVER);
			conn = DriverManager
					.getConnection(URL,USER, PASS);

			// prepare query
			String query = "SELECT review FROM reviews_restaurants order by business_id;";

			// create the java statement
			Statement st = conn.createStatement();

			// execute the query, and get a java resultset
			ResultSet rs = st.executeQuery(query);

			// iterate through the java resultset
			while (rs.next())
			{
				review = rs.getString("review");
				if (review != null) {
					reviews.add(review);
				}

			}
			conn.close();		

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return reviews;
	} // method end



	/**
	 * Read 1 in n reviews JSON file and insert in database (table reviews)
	 */
	public static void readReviews(int n) {   	    	

		JSONParser parser = new JSONParser();

		String file =  "D:\\DataMiningCapstone\\"
				+ 	"yelp_dataset_challenge_academic_dataset\\"
				+ 	"yelp_academic_dataset_review.json";

		ArrayList<String> business_list = new ArrayList<String>();
		ArrayList<String> texts = new ArrayList<String>();
		ArrayList<String> starsList = new ArrayList<String>();

		int k = 0;	
		
		String text = null;
		String business_id = null;
		String stars = null;
		JSONObject jsonObject = null;

		try (BufferedReader br = new BufferedReader(new FileReader(file))) {

			String line;
			while ((line = br.readLine()) != null) {
				if (k == n ) {	
					// process the line           	
					jsonObject = (JSONObject) parser.parse(line);
					
					// Fields extracted from JSON
					business_id = (String) jsonObject.get("business_id");
					text = (String) jsonObject.get("text");
					stars =   String.valueOf( 
								((Long) jsonObject.get("stars")).longValue());
					
					// add to list to be inserted to DB
					business_list.add(business_id);
					texts.add(text);
					starsList.add(stars);

					k = 0;
				}
				k++;
			}

			// insert in restaurant table
			String[] cols = {"business_id","review","stars"};

			ArrayList<ArrayList<String>> lists = new  ArrayList<ArrayList<String>>();
			lists.add(business_list);
			lists.add(texts);
			lists.add(starsList);

			DataMiningUtil.insertData("reviews", cols, lists);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}    	

	} // method end
}
